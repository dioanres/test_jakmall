<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;

class multiply extends Command
{
    protected $signature = 'multiply {numbers*}';

    protected $description = 'Multiply All Given Number';

    protected $usage = "multiply <numbers>";

    public function __construct() 
    {
        parent::__construct();
    }

    public function handle() {
        $numbers = $this->argument('numbers');
        $result = $numbers[0] ;
        
        $arr_lengt = count($numbers);

        for ($i=1; $i < $arr_lengt ; $i++) { 
            $result = $result * $numbers[$i];
        }
        
        echo $result;
    }
}
