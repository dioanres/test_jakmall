<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;

class substract extends Command
{
    protected $signature = 'substract {numbers*}';

    protected $description = 'Substract All Given Number';

    protected $usage = "substract <numbers>";

    public function __construct() 
    {
        parent::__construct();
    }

    public function handle() {
        $numbers = $this->argument('numbers');
        $result = $numbers[0] ;
        $subs = '';

        $arr_lengt = count($numbers);

        for ($i=1; $i < $arr_lengt ; $i++) { 
            $result -= $numbers[$i];
        }

        echo $result;
    }
}
