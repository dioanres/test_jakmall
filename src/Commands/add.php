<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;

class add extends Command
{
    protected $signature = 'add {numbers*}';

    protected $description = 'Add All Given Number';

    protected $usage = "add <numbers>";

    public function __construct() 
    {
        parent::__construct();
    }

    public function handle() {
        $a = $this->argument('numbers');
        $result = 0 ;
        

        foreach ($a as $key => $value) {
            
            $result = $result + $value;
        }
        
        echo $result;
    }
}
