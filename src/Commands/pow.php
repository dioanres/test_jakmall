<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;

class pow extends Command
{
    protected $signature = 'pow {base} {exp}';

    protected $description = 'Exponent the Given Number';

    protected $usage = "pow <base> <exp>";

    public function __construct() 
    {
        parent::__construct();
    }

    public function handle() {
        $base = $this->argument('base');
        $exp = $this->argument('exp');
        $result = 0 ;

        $result = pow($base,$exp);
        
        echo $result;
    }
}
